﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TRANSLATOR
{
    public class MicrosoftTranslator
    {
        private static AdmAuthentication admAuth = new AdmAuthentication(Properties.Settings.Default.ClientId, Properties.Settings.Default.ClientSecret);
        private static readonly string bearer = "Bearer ";
        private static readonly string authorization = "Authorization";

        public string Translate(string fromLanguage, string toLanguage, string text)
        {
            string authToken = bearer + admAuth.AccessToken.access_token;
            string uri = String.Format(Properties.Settings.Default.TranslateApi, System.Web.HttpUtility.UrlEncode(text), fromLanguage, toLanguage);
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(authorization, authToken);
            WebResponse response = null;
            try
            {
                response = httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(System.String));
                    string translatedText = dcs.ReadObject(stream).ToString();
                    return translatedText;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
            return String.Empty;
        }
    }
}
