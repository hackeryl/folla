﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class GamesService
    {
        private DAL.UnitOfWork unit;

        public GamesService()
        {
            this.unit = new DAL.UnitOfWork();
        }

        public IEnumerable<DAL.Game> GetAllGames()
        {
            return unit.Get<DAL.Game>();
        }

        public DAL.Game GetGameById(int gameId)
        {
            return unit.Get<DAL.Game>(a => a.GameId.Equals(gameId)).FirstOrDefault();
        }

        public IEnumerable<DAL.Game> GetGamesByLanguageId(int languageId)
        {
            return unit.Get<DAL.Game>(a => a.LanguageId.Equals(languageId));
        }
    }
}
