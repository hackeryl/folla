﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class UserService
    {
        private DAL.UnitOfWork unit;
        private EmailValidator validator;

        public UserService()
        {
            this.unit = new DAL.UnitOfWork();
            this.validator = new EmailValidator();
        }

        public DAL.User GetUserByUsername(string username)
        {
            return unit.Get<DAL.User>(a => a.Username.Equals(username)).FirstOrDefault();
        }

        public DAL.User GetUserById(int userId)
        {
            return unit.Get<DAL.User>(a => a.UserId.Equals(userId)).FirstOrDefault();
        }

        public DAL.User GetUserByEmail(string email)
        {
            return unit.Get<DAL.User>(a => a.Email.Equals(email)).FirstOrDefault();
        }

        public IEnumerable<DAL.Achievement> GetUserAchievements(int userId)
        {
            var user = GetUserById(userId);
            if (user != null)
            {
                return user.UserAchievements.Select(a=>a.Achievement);
            }
            else
            {
                return new List<DAL.Achievement>();
            }
        }

        public IEnumerable<DAL.SocialAccount> GetUserSocialAccounts(int userId)
        {
            var user = GetUserById(userId);
            if (user != null)
            {
                return user.SocialAccounts;
            }
            else
            {
                return new List<DAL.SocialAccount>();
            }
        }

        public IEnumerable<DAL.SocialNetwork> GetAllSocialNetworks()
        {
            return unit.Get<DAL.SocialNetwork>();
        }

        public int GetNumberOfPassedLessons(int userId)
        {
            var user = GetUserById(userId);
            if (user != null)
            {
                return user.LessonSolutions.Count(a=>a.CorrectAnswers.Equals(a.Lesson.Questions.Count));
            }
            else
            {
                return 0;
            }
        }

        public int GetNumberOfNotCompletedLessons(int userId)
        {
            var user = GetUserById(userId);
            if (user != null)
            {
                return user.LessonSolutions.Count - user.LessonSolutions.Count(a => a.CorrectAnswers.Equals(a.Lesson.Questions.Count));
            }
            else
            {
                return 0;
            }
        }

        public int GetUserAccurancy(int userId)
        {
            var user = GetUserById(userId);
            if (user != null)
            {
                int correctAnswers = user.LessonSolutions.Sum(a => a.CorrectAnswers);
                int totalQuestions = user.LessonSolutions.Sum(a => a.Lesson.Questions.Count);
                if (totalQuestions == 0)
                {
                    return 0;
                }
                return (correctAnswers * 100) / totalQuestions;
            }
            else
            {
                return 0;
            }
        }

        public bool ExistsUser(string username, string password, out DAL.User user)
        {
            user = unit.Get<DAL.User>(a => a.Username.Equals(username) && a.Password.Equals(password)).FirstOrDefault();
            return user != null;
        }

        public bool RegisterUser(string username, string email, string password, out DAL.User user)
        {
            if (String.IsNullOrWhiteSpace(username) || String.IsNullOrWhiteSpace(email) || String.IsNullOrWhiteSpace(password)
                || username.Length < 4 || password.Length < 4 || !validator.IsValidEmail(email))
            {
                user = null;
                return false;
            }
            var existingUser = GetUserByUsername(username);
            if (existingUser == null)
            {
                user = new DAL.User()
                {
                    Username = username,
                    Password = password,
                    Email = email
                };
                unit.Insert<DAL.User>(user);
                unit.Save();
                return true;
            }
            else
            {
                user = null;
                return false;
            }
        }
    }
}
