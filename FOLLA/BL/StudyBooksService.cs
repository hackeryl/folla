﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class StudyBooksService
    {
        private DAL.UnitOfWork unit;

        public StudyBooksService()
        {
            this.unit = new DAL.UnitOfWork();
        }

        public IEnumerable<DAL.StudyBook> GetAllStudyBooks()
        {
            return unit.Get<DAL.StudyBook>();
        }

        public DAL.StudyBook GetStudyBookById(int studyBookId)
        {
            return unit.Get<DAL.StudyBook>(a => a.StudyBookId.Equals(studyBookId)).FirstOrDefault();
        }

        public IEnumerable<DAL.StudyBook> GetStudyBooksByLanguageId(int languageId)
        {
            return unit.Get<DAL.StudyBook>(a => a.LanguageId.Equals(languageId));
        }
    }
}
