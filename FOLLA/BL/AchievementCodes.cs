﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    enum AchievementCodes : int
    {
        FirstLessonCompleted = 1,
        LevelCompleted = 2,
        LessonCategoryCompleted = 3
    }
}
