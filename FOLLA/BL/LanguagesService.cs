﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class LanguagesService
    {
        private DAL.UnitOfWork unit;
        private TRANSLATOR.MicrosoftTranslator translator;

        public LanguagesService()
        {
            this.unit = new DAL.UnitOfWork();
            this.translator = new TRANSLATOR.MicrosoftTranslator();
        }

        public IEnumerable<DAL.Language> GetAllLanguages()
        {
            return unit.Get<DAL.Language>();
        }

        public DAL.Language GetLanguageById(int languageId)
        {
            return unit.Get<DAL.Language>(a => a.LanguageId.Equals(languageId)).FirstOrDefault();
        }

        public DAL.Language GetLanguageByName(string languageName)
        {
            return unit.Get<DAL.Language>(a => a.Name.Equals(languageName)).FirstOrDefault();
        }

        public IEnumerable<DAL.Level> GetAllLevels()
        {
            return unit.Get<DAL.Level>();
        }

        public IEnumerable<DAL.Level> GetAllLevelsByLanguageId(int languageId)
        {
            var language = GetLanguageById(languageId);
            if (language != null)
            {
                return language.Lessons.Select(a => a.Level).Distinct();
            }
            else
            {
                return new List<DAL.Level>();
            }
        }

        public IEnumerable<DAL.LessonCategory> GetAllLessonCategoriesByLanguageId(int languageId)
        {
            var language = GetLanguageById(languageId);
            if (language != null)
            {
                return language.Lessons.Select(a => a.LessonCategory).Distinct();
            }
            else
            {
                return new List<DAL.LessonCategory>();
            }
        }

        public string Translate(int fromLanguageId, int toLanguageId, string text)
        {
            var fromLanguage = unit.Get<DAL.Language>(a => a.LanguageId.Equals(fromLanguageId)).FirstOrDefault();
            var toLanguage = unit.Get<DAL.Language>(a => a.LanguageId.Equals(toLanguageId)).FirstOrDefault();
            if (fromLanguage != null && toLanguage != null)
            {
                string translatedText = translator.Translate(fromLanguage.Code, toLanguage.Code, text);
                return translatedText;
            }
            else
            {
                return String.Empty;
            }
        }
    }
}
