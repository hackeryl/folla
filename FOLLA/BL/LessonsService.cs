﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class LessonsService
    {
        private DAL.UnitOfWork unit;

        public LessonsService()
        {
            this.unit = new DAL.UnitOfWork();
        }

        public IEnumerable<DAL.Lesson> GetAllLessons()
        {
            return unit.Get<DAL.Lesson>();
        }

        public DAL.Lesson GetLessonById(int lessonId)
        {
            return unit.Get<DAL.Lesson>(a => a.LessonId.Equals(lessonId)).FirstOrDefault();
        }

        public IEnumerable<DAL.Lesson> GetLessonsByLevelId(int languageId, int levelId)
        {
            return unit.Get<DAL.Lesson>(a => a.LanguageId.Equals(languageId) && a.LevelId.Equals(levelId));
        }

        public IEnumerable<DAL.Lesson> GetLessonsByCategoryId(int languageId, int lessonCategoryId)
        {
            return unit.Get<DAL.Lesson>(a => a.LanguageId.Equals(languageId) && a.LessonCategoryId.Equals(lessonCategoryId));
        }

        public IEnumerable<DAL.Question> GetLessonQuestionsByLessonId(int lessonId)
        {
            var lesson = unit.Get<DAL.Lesson>(a => a.LessonId.Equals(lessonId), null, "Questions").FirstOrDefault();
            if (lesson != null)
            {
                var questions = lesson.Questions.ToList();
                foreach (var q in questions)
                {
                    q.Lesson = null;
                }
                return questions;
            }
            else
            {
                return new List<DAL.Question>();
            }
        }

        public IEnumerable<DAL.Lesson> GetLessonsByCategoryAndLevel(int languageId, int lessonCategoryId, int levelId)
        {
            return unit.Get<DAL.Lesson>(a => a.LanguageId.Equals(languageId) && a.LessonCategoryId.Equals(lessonCategoryId) && a.LevelId.Equals(levelId));
        }

        public IEnumerable<DAL.Lesson> GetLessonsByLanguageId(int languageId)
        {
            return unit.Get<DAL.Lesson>(a => a.LanguageId.Equals(languageId));
        }

        public Dictionary<int, bool> GetLessonsResults(int userId)
        {
            Dictionary<int, bool> result = new Dictionary<int, bool>();
            var solutions = unit.Get<DAL.LessonSolution>(a => a.UserId.Equals(userId)).Select(a => new { LessonId = a.LessonId, Passed = a.CorrectAnswers == a.Lesson.Questions.Count });
            foreach (var solution in solutions)
            {
                result[solution.LessonId] = solution.Passed;
            }
            return result;
        }

        public bool SaveLessonSolution(DAL.LessonSolution lessonSolution)
        {
            try
            {
                var existingSolution = unit.Get<DAL.LessonSolution>(a => a.UserId.Equals(lessonSolution.UserId) && a.LessonId.Equals(lessonSolution.LessonId)).FirstOrDefault();
                var user = unit.Get<DAL.User>(a => a.UserId.Equals(lessonSolution.UserId)).FirstOrDefault();
                if (existingSolution == null)
                {
                    unit.Insert<DAL.LessonSolution>(lessonSolution);
                    user.Score = user.Score + lessonSolution.CorrectAnswers;
                }
                else
                {
                    user.Score = user.Score - existingSolution.CorrectAnswers + lessonSolution.CorrectAnswers;
                    existingSolution.CorrectAnswers = lessonSolution.CorrectAnswers;
                }
                unit.Save();
                UpdateUserAchievements(user.UserId);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private void UpdateUserAchievements(int userId)
        {
            var user = unit.Get<DAL.User>(a => a.UserId.Equals(userId)).FirstOrDefault();
            if (user != null)
            {
                var levelSolutions = from solution in unit.Get<DAL.LessonSolution>(a => a.UserId.Equals(user.UserId))
                                     group solution by solution.Lesson.Level into levelSol
                                     select levelSol;
                var categorySolutions = from solution in unit.Get<DAL.LessonSolution>(a => a.UserId.Equals(user.UserId))
                                        group solution by solution.Lesson.LessonCategory into categorySol
                                        select categorySol;
                bool levelCompleted = false;
                foreach (var level in levelSolutions)
                {
                    if (level.Key.Lessons.Count == level.Count())
                    {
                        levelCompleted = true;
                        break;
                    }
                }
                bool categoryCompleted = false;
                foreach (var category in categorySolutions)
                {
                    if (category.Key.Lessons.Count == category.Count())
                    {
                        categoryCompleted = true;
                        break;
                    }
                }
                bool firstLessonCompleted = false;
                if (user.LessonSolutions.Count(a => a.CorrectAnswers.Equals(a.Lesson.Questions.Count)) > 0)
                {
                    firstLessonCompleted = true;
                }

                if (firstLessonCompleted && user.UserAchievements.Count(a => a.AchievementId.Equals((int)AchievementCodes.FirstLessonCompleted)) == 0)
                {
                    var firstLessonCompletedAchievement = unit.Get<DAL.Achievement>(a => a.AchievementId.Equals((int)AchievementCodes.FirstLessonCompleted)).FirstOrDefault();
                    var firstLessonCompletedUserAchievement = new DAL.UserAchievement()
                    {
                        AchievementId = firstLessonCompletedAchievement.AchievementId,
                        UserId = user.UserId
                    };
                    user.UserAchievements.Add(firstLessonCompletedUserAchievement);
                }

                if (categoryCompleted && user.UserAchievements.Count(a => a.AchievementId.Equals((int)AchievementCodes.LessonCategoryCompleted)) == 0)
                {
                    var lessonCategoryCompletedAchievement = unit.Get<DAL.Achievement>(a => a.AchievementId.Equals((int)AchievementCodes.LessonCategoryCompleted)).FirstOrDefault();
                    var lessonCategoryCompletedUserAchievement = new DAL.UserAchievement()
                    {
                        AchievementId = lessonCategoryCompletedAchievement.AchievementId,
                        UserId = user.UserId
                    };
                    user.UserAchievements.Add(lessonCategoryCompletedUserAchievement);
                }

                if (levelCompleted && user.UserAchievements.Count(a => a.AchievementId.Equals((int)AchievementCodes.LevelCompleted)) == 0)
                {
                    var levelCompletedAchievement = unit.Get<DAL.Achievement>(a => a.AchievementId.Equals((int)AchievementCodes.LevelCompleted)).FirstOrDefault();
                    var levelCompletedUserAchievement = new DAL.UserAchievement()
                    {
                        AchievementId = levelCompletedAchievement.AchievementId,
                        UserId = user.UserId
                    };
                    user.UserAchievements.Add(levelCompletedUserAchievement);
                }
                unit.Save();
            }
        }
    }
}
