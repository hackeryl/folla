﻿function Question_Sound_Text(data, imgPath, sndPath) {
    Question.call(this, data);
    this.soundsPath = sndPath;
};

Question_Sound_Text.prototype = Object.create(Question.prototype);

Question_Sound_Text.prototype.constructor = Question_Sound_Text;

Question_Sound_Text.prototype.renderQuestion = function (container) {
    Question.prototype.renderQuestion.call(this, container);

    var div = document.createElement("div");
    div.classList.add("question-media");
    var button = document.createElement("button");
    button.classList.add("sound-btn");
    var img = document.createElement("img");
    img.src = "/Images/sound-icon.png";
    img.alt = "sound";
    button.appendChild(img);
    button.onclick = (function (src) {
        return function () {
            AudioPlayer.playSound(src);
        }
    })(this.soundsPath + this.resourceUrl);
    div.appendChild(button);

    container.appendChild(div);
};

Question_Sound_Text.prototype.renderAnswer = function (answerIndex, container) {
    Question.prototype.renderAnswer.call(this, answerIndex, container);
};