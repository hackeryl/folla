﻿function Question_Text_Text(data) {
    Question.call(this, data);
};

Question_Text_Text.prototype = Object.create(Question.prototype);

Question_Text_Text.prototype.constructor = Question_Text_Text;

Question_Text_Text.prototype.renderQuestion = function (container) {
    Question.prototype.renderQuestion.call(this, container);
};

Question_Text_Text.prototype.renderAnswer = function (answerIndex, container) {
    Question.prototype.renderAnswer.call(this, answerIndex, container);
};