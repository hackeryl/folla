﻿function Question_Text_Image(data, imgPath) {
    Question.call(this, data);
    this.imagesPath = imgPath;
};

Question_Text_Image.prototype = Object.create(Question.prototype);

Question_Text_Image.prototype.constructor = Question_Text_Image;

Question_Text_Image.prototype.renderQuestion = function (container) {
    Question.prototype.renderQuestion.call(this, container);
};

Question_Text_Image.prototype.renderAnswer = function (answerIndex, container) {
    var img = document.createElement("img");
    img.src = this.imagesPath + this.answers[answerIndex - 1];
    img.alt = this.text;

    container.innerHTML = "";
    container.appendChild(img);

    Question.prototype.highlightAnswerContainer.call(this, answerIndex, container);
};