﻿//LocalUserStats - used for populating guest user stats
var localUserStatsLoader = (function () {
    var self = {};

    //Replacing share links parameter {user_score} with the actuall value
    function setSocialNetworksLink(score) {
        var socialLinks = $('.social-network-link');
        for (var i = 0, size = socialLinks.length; i < size; i++) {
            var socialLink = socialLinks[i];
            var href = $(socialLink).attr('href');
            href = href.replace("{user_score}", score);
            $(socialLink).attr('href', href);
        }
    }

    //Returns user stats(lessonsCompleted,lessonsFailed,accuracy)
    function setUserStats() {
        var stats = ProgressStorage.getUser("GUEST").getStatsData();
        $('#lessonsCompleted').html(stats.lessonsCompleted);
        $('#lessonsFailed').html(stats.lessonsFailed);
        $('#userAccurancy').html(stats.accuracy + "%");
        $('#score').html(stats.score);
        setSocialNetworksLink(stats.score);
    }

    self.loadLocalUserStats = function() {
        if (!__AUTHENTIFICATED__) {
            setUserStats();
        }
    }

    return self;
})();

$(document).ready(localUserStatsLoader.loadLocalUserStats);
