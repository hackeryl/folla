﻿//Lesson - used for managing the Lesson questions, answers, score
var Lesson = (function (lesson) {
    var prevBtn;
    var nextBtn;
    var questionContainer;
    var answerContainers;
    var progressBar;
    var correctCountDiv;

    var lessonId;
    var questions;
    var currentQuestionIndex;
    var correctAnsweredQuestions;
    var questionsAnswersOrder;
    var imagesPath;
    var soundsPath;
    var homePath;

    //sets the current question index to progressbar
    function setCurrentQuestionIndex(index) {
        if (index >= 0 && index < questions.length) {
            currentQuestionIndex = index;
            progressBar.value = currentQuestionIndex + 1;
        }
        if (currentQuestionIndex == questions.length - 1) {
            nextBtn.innerHTML = "Sumbit";
            nextBtn.classList.toggle("correctAnswer", true);
            nextBtn.onclick = submit;
        }
        else {
            nextBtn.innerHTML = "Next";
            nextBtn.classList.toggle("correctAnswer", false);
            nextBtn.onclick = nextQuestion;
        }
    };

    //sets the number of correct questions count in the box
    function setCorrectQuestionsCount(value) {
        correctAnsweredQuestions = value;
        correctCountDiv.innerHTML = value + "/" + questions.length;
    };

    //button handlers assigned
    function assignHandlers() {
        prevBtn.onclick = prevQuestion;
        nextBtn.onclick = nextQuestion;
    };

    //assings Answer handler
    function assignAnswerHandler(answerContainer, answerIndex) {
        answerContainer.onclick = function (answerIndex) {
            return function () {
                answer(answerIndex);
            };
        }(answerIndex);
    };

    //highlights answer when user answered a question
    function highlightAnswers() {
        showQuestion(currentQuestionIndex);
    };

    //creates questions based on data received from service
    function initializeQuestions(data) {
        questions = [];
        if (data) {
            for (var i in data) {
                var question = QuestionsFactory.createQuestion(data[i], imagesPath, soundsPath);
                questions.push(question);
            }
        }
    };

    //initializes questions answers oreded to be displayed in containers
    function initializeQuestionsOrders() {
        questionsAnswersOrder = [];
        for (var i in questions) {
            questionsAnswersOrder[i] = getRandomArray(1, questions[i].answers.length);
        }
    };

    //return the number of correct answered questions
    function getNumberOfCorrectAnswers() {
        var correctAnswers = 0;
        for (var i in questions) {
            if (questions[i].isCorrectAnswered() === true) {
                correctAnswers++;
            }
        }
        return correctAnswers;
    };

    //return a random array which contains numbers 'fromNumber' to 'toNumber'
    function getRandomArray(fromNumber, toNumber) {
        var index = [];
        var values = [];

        for (var i = fromNumber; i <= toNumber; i++) {
            var number = Math.floor(Math.random() * (toNumber - fromNumber)) + fromNumber;
            values[i] = {};
            values[i].Number = number;
            values[i].Index = i;
        }

        values.sort(function (a, b) {
            return a.Number - b.Number;
        });

        for (var i in values) {
            index[i] = values[i].Index;
        }

        return index;
    };

    //returns a random string of length = length
    function getRandomString(length) {
        var result = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < length; i++)
            result += possible.charAt(Math.floor(Math.random() * possible.length));

        return result;
    };

    //Renders the index'th question
    function showQuestion(index) {
        if (index >= 0 && index < questions.length) {
            var question = questions[index];
            question.renderQuestion(questionContainer);
            var answersOrderIdxs = questionsAnswersOrder[index];
            for (var i = 0; i < answersOrderIdxs.length; i++) {
                question.renderAnswer(answersOrderIdxs[i], answerContainers[i]);
                assignAnswerHandler(answerContainers[i], answersOrderIdxs[i]);
            }
        }
    };

    //Starts the lesson
    function startLesson() {
        progressBar.max = questions.length;
        setCurrentQuestionIndex(0);
        setCorrectQuestionsCount(0);
        showQuestion(currentQuestionIndex);
    };

    //Next button handler - goes to next question in lesson
    function nextQuestion() {
        if (currentQuestionIndex < questions.length - 1) {
            setCurrentQuestionIndex(currentQuestionIndex + 1);
            showQuestion(currentQuestionIndex);
        }
    };

    //Previous button handler - goes to previous question in lesson
    function prevQuestion() {
        if (currentQuestionIndex > 0) {
            setCurrentQuestionIndex(currentQuestionIndex - 1);
            showQuestion(currentQuestionIndex);
        }
    };

    //Quesitons Loaded handler
    function onQuestionsLoaded(data) {
        initializeQuestions(data);
        initializeQuestionsOrders();
        startLesson();
    };

    //Solution Submitted Handler
    function onSolutionSubmitted(data) {
        if (data) {
            Dialog.open("Succes", "Answers submitted succesfully!", onDialogClosed);
        }
        else {
            Dialog.open("Error", "Something went wrong, please try again!");
        }
    };

    //Dialog closed handler
    function onDialogClosed(event, ui) {
        var randomString = getRandomString(7);
        window.location.href = homePath + "?r=" + randomString;
    }

    //Submits results using the service
    function submit() {
        var lessonSolutionRequest = {};
        lessonSolutionRequest.LessonId = lessonId;
        lessonSolutionRequest.CorrectAnswers = getNumberOfCorrectAnswers();
        lessonSolutionRequest.NumberOfQuestions = questions.length;
        follaService.submitLessonSolution(lessonSolutionRequest).done(onSolutionSubmitted);
    };

    //Answer handler
    function answer(answerIndex) {
        var question = questions[currentQuestionIndex];
        if (question) {
            var result = question.answer(answerIndex);
            if (result !== null) {
                if (result === true) {
                    setCorrectQuestionsCount(correctAnsweredQuestions + 1);
                }
                highlightAnswers();
            }
        }
    };

    //Initializing global variables
    lesson.Global_Init = function (imgPath, sndPath, indexPath) {
        imagesPath = imgPath;
        soundsPath = sndPath;
        homePath = indexPath;
        prevBtn = document.getElementById("prev-btn");
        nextBtn = document.getElementById("next-btn");
        questionContainer = document.getElementById("question-div");
        progressBar = document.getElementById("progress-bar");
        correctCountDiv = document.getElementById("correctCount-div");
        answerContainers = [];
        for (var i = 1; i <= 4; i++) {
            answerContainers.push(document.getElementById("ans" + i));
        };
        assignHandlers();
    };

    //Starts the lesson
    lesson.Start = function (_lessonId) {
        lessonId = _lessonId;
        var getQuestionsData = {};
        getQuestionsData.lessonId = _lessonId;
        follaService.getQuestionsByLessonId(getQuestionsData).done(onQuestionsLoaded);
    };

    return lesson;
})(Lesson || {});