﻿//Audio Player for playing questions
var AudioPlayer = (function (self) {

    //Plays a sound with a specified url
    self.playSound = function (src) {
        var sound = new Audio(src);
        sound.play();
    };

    return self;
})(AudioPlayer || {});