﻿function Question_Sound_Image(data, imgPath, sndPath) {
    Question.call(this, data);
    this.imagesPath = imgPath;
    this.soundsPath = sndPath;
};

Question_Sound_Image.prototype = Object.create(Question.prototype);

Question_Sound_Image.prototype.constructor = Question_Sound_Image;

Question_Sound_Image.prototype.renderQuestion = function (container) {
    Question.prototype.renderQuestion.call(this, container);

    var div = document.createElement("div");
    div.classList.add("question-media");
    var button = document.createElement("button");
    button.classList.add("sound-btn");
    var img = document.createElement("img");
    img.src = "/Images/sound-icon.png";
    img.alt = "sound";
    button.appendChild(img);
    button.onclick = (function (src) {
        return function () {
            AudioPlayer.playSound(src);
        }
    })(this.soundsPath + this.resourceUrl);
    div.appendChild(button);

    container.appendChild(div);
};

Question_Sound_Image.prototype.renderAnswer = function (answerIndex, container) {
    var img = document.createElement("img");
    img.src = this.imagesPath + this.answers[answerIndex - 1];
    img.alt = this.text;

    container.innerHTML = "";
    container.appendChild(img);

    Question.prototype.highlightAnswerContainer.call(this, answerIndex, container);
};