﻿//Question - base class for questions
function Question(data) {
    this.text = data.Text;
    this.lessonId = data.LessonId;
    this.type = data.Type;
    this.resourceUrl = data.ResourceUrl;
    this.correctAnswerId = data.CorrectAnswerId;
    this.answers = [];
    for (var i = 1; i <= 4; i++) {
        if (data["Answer" + i]) {
            this.answers.push(data["Answer" + i]);
        }
    };
    this.answered = false;
    this.answeredId = -1;
};

//Renders the question in a specified container
Question.prototype.renderQuestion = function (container) {
    var h3 = document.createElement("h3");
    h3.classList.add("question-text");
    var text = document.createTextNode(this.text);
    h3.appendChild(text);
    
    container.innerHTML = "";
    container.appendChild(h3);
};

//Renders the answer in a specified container
Question.prototype.renderAnswer = function (answerIndex, container) {
    container.innerHTML = this.answers[answerIndex - 1];
    Question.prototype.highlightAnswerContainer.call(this, answerIndex, container);
};

//Method for answering the question
Question.prototype.answer = function (answerId) {
    if (!this.answered && answerId && answerId >= 1 && answerId <= this.answers.length) {
        this.answered = true;
        this.answeredId = answerId;
        return (this.correctAnswerId == answerId);
    }
    else {
        return null;
    }
};

//Returns : true if it's correct answered, false otherwise
Question.prototype.isCorrectAnswered = function () {
    return this.correctAnswerId === this.answeredId;
};

//HighLights correct and incorrect answers in container
Question.prototype.highlightAnswerContainer = function (answerIndex, container) {
    if (this.answered) {
        if (this.correctAnswerId == answerIndex) {
            container.classList.toggle("correctAnswer", true);
        }
        else if (this.answeredId != this.correctAnswerId && this.answeredId == answerIndex) {
            container.classList.toggle("wrongAnswer", true);
        }
        else {
            container.classList.toggle("correctAnswer", false);
            container.classList.toggle("wrongAnswer", false);
        }
    }
    else {
        container.classList.toggle("correctAnswer", false);
        container.classList.toggle("wrongAnswer", false);
    }
};