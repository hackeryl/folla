﻿//Dictionary - used to translate text from one language to other(Dictionary Widget)
var dictionary = (function () {
    var self = {};

    //attacching events
    self.attachEvents = function () {
        $("#translate-btn").on('click', onTranslateBtnClick);
    };

    //Translate button click handler
    function onTranslateBtnClick(event) {
        event.preventDefault();
        var inputText = $('#input-lang-box').val();
        if (inputText.length != 0) {
            var inputLangId = $('#input-lang-select').val();
            var outputLangId = $('#output-lang-select').val();
            var dictionaryData = {};
            dictionaryData.fromLanguageId = inputLangId;
            dictionaryData.toLanguageId = outputLangId;
            dictionaryData.text = inputText;
            follaService.translate(dictionaryData).done(onTranslationDone);
        }
    };

    //Translation done handler
    function onTranslationDone(data) {
        $('#output-lang-box').val(data);
    };
    return self;

})();

$(document).ready(dictionary.attachEvents);

