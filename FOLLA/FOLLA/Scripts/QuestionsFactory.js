﻿//Questions Factory - Create Question Objects based on their type
var QuestionsFactory = (function (factory) {
    var creators = [];

    //Registers All Questions Constructors
    var registerConstructors = function () {
        creators[0] = Question; //Default
        creators[1] = Question_Text_Text;
        creators[2] = Question_Text_Image;
        creators[3] = Question_Image_Text;
        creators[4] = Question_Sound_Text;
        creators[5] = Question_Sound_Image;
    };

    //Returns a new Question based on it's type
    factory.createQuestion = function (data, imgPath, sndPath) {
        if (data && data["Type"]) {
            return new creators[data.Type](data, imgPath, sndPath);
        }
        else {
            return new creators[0](data); //Default Constructor
        }
    }

    registerConstructors();

    return factory;
})(QuestionsFactory || {});