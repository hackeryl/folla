﻿function Question_Image_Text(data, imgPath) {
    Question.call(this, data);
    this.imagesPath = imgPath;
};

Question_Image_Text.prototype = Object.create(Question.prototype);

Question_Image_Text.prototype.constructor = Question_Image_Text;

Question_Image_Text.prototype.renderQuestion = function (container) {
    Question.prototype.renderQuestion.call(this, container);

    var div = document.createElement("div");
    div.classList.add("question-media");
    var img = document.createElement("img");
    img.src = this.imagesPath + this.resourceUrl;
    img.alt = this.text;
    div.appendChild(img);

    container.appendChild(div);
};

Question_Image_Text.prototype.renderAnswer = function (answerIndex, container) {
    Question.prototype.renderAnswer.call(this, answerIndex, container);
};