﻿//Dialog - used for displaying success/error/info messages
var Dialog = (function (dialog) {
    var dialog;

    //initialization from div
    function init() {
        $("#dialog").dialog({
            autoOpen: false,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            },
            show: {
                duration: 250
            },
            hide: {
                duration: 250
            }
        });
    }

    //Sets the message
    function setMessage(message) {
        document.getElementById("dialog-message").innerHTML = message;
    };

    //Sets the title
    function setTitle(title) {
        document.getElementById("dialog").title = title;
    };

    //Opens the dialog with specified title, message, and calls onCloseCallback when it's closed
    dialog.open = function (title, message, onCloseCallback) {
        if (title) {
            setTitle(title);
        }
        else {
            setTitle("Info");
        }
        if (message) {
            setMessage(message);
        }
        else {
            setMessage("");
        }
        if (onCloseCallback) {
            $("#dialog").on("dialogclose", function () {
                $("#dialog").unbind("dialogclose", onCloseCallback);
                onCloseCallback();
            });
        }
        $("#dialog").dialog("open");
    };

    init();

    return dialog;
})(Dialog || {});