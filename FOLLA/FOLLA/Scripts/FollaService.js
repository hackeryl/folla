﻿//Service for requesting data from server/localstorage
follaService = (function () {
    var self = {};

    //Login method
    self.login = function (loginData) {
        return $.ajax({
            url: "/Home/Login",
            type: "POST",
            data: loginData
        });
    };

    //Logout method
    self.logout = function () {
        return $.ajax({
            url: "/Home/Logout",
            type: "POST"
        });
    };

    //Register Metod
    self.register = function (registerData) {
        return $.ajax({
            url: "/Home/Register",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            mimeType: "application/json",
            data: JSON.stringify(registerData)
        });
    };

    //Get All Questions for a specified lesson
    self.getQuestionsByLessonId = function (getQuestionsData) {
        return $.ajax({
            url: "/Home/GetQuestionsByLessonId",
            type: "GET",
            data: getQuestionsData
        });
    };

    //Submit Lesson sollution to server/localstorage
    self.submitLessonSolution = function (lessonSolutionData) {
        if (__AUTHENTIFICATED__) {
            return submitLessonSolution_Server(lessonSolutionData);
        }
        else {
            return submitLessonSolution_Local(lessonSolutionData);
        }
    };

    //Save Lesson Solution to localstorage
    function submitLessonSolution_Local(lessonSolutionData) {
        var result = {};
        var data = ProgressStorage.saveLessonSolutionForUser(lessonSolutionData, "GUEST");
        result.done = function (callback) {
            callback(data);
        };
        return result;
    };


    //Post Lesson Solution to server
    function submitLessonSolution_Server(lessonSolutionData) {
        return $.ajax({
            url: "/Home/LessonSolution",
            type: "POST",
            data: lessonSolutionData
        });
    };

    //Dictionary translate method
    self.translate = function (translateData) {
        return $.ajax({
            url: "/Home/Translate",
            type: "GET",
            data: translateData
        });
    }
    
    return self;
})();
