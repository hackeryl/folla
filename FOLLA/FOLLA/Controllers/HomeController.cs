﻿using FOLLA.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL;

namespace FOLLA.Controllers
{
    public class HomeController : Controller
    {
        private UserService _usersService = new UserService();
        private LanguagesService _languagesService = new LanguagesService();
        private LessonsService _lessonsService = new LessonsService();

        [HttpGet]
        public ActionResult Index(int? languageId)
        {
            if (languageId.HasValue)
            {
                SessionHelper.CurrentLanguageId = languageId.Value;
            }
            if (SessionHelper.CurrentLanguageId != -1)
            {
                ViewBag.Levels = _languagesService.GetAllLevelsByLanguageId(SessionHelper.CurrentLanguageId);
                ViewBag.Categories = _languagesService.GetAllLessonCategoriesByLanguageId(SessionHelper.CurrentLanguageId);
            }
            else
            {
                ViewBag.Languages = _languagesService.GetAllLanguages();
            }
            ViewBag.LanguageSelected = SessionHelper.CurrentLanguageId != -1;
            return View();
        }

        [HttpGet]
        public ActionResult Lessons(int? categoryId, int? levelId)
        {
            if (SessionHelper.CurrentLanguageId == -1)
            {
                return RedirectToAction("Index");
            }
            if (levelId.HasValue && categoryId.HasValue)
            {
                ViewBag.Lessons = _lessonsService.GetLessonsByCategoryAndLevel(SessionHelper.CurrentLanguageId, categoryId.Value, levelId.Value);
            }
            else if (categoryId.HasValue)
            {
                ViewBag.Lessons = _lessonsService.GetLessonsByCategoryId(SessionHelper.CurrentLanguageId, categoryId.Value);
            }
            else if (levelId.HasValue)
            {
                ViewBag.Lessons = _lessonsService.GetLessonsByLevelId(SessionHelper.CurrentLanguageId, levelId.Value);
            }
            else
            {
                ViewBag.Lessons = _lessonsService.GetLessonsByLanguageId(SessionHelper.CurrentLanguageId);
            }
            ViewBag.CompletedLessons = _lessonsService.GetLessonsResults(SessionHelper.UserId);
            return View();
        }

        [HttpGet]
        public ActionResult Lesson(int? lessonId)
        {
            if (lessonId.HasValue)
            {
                var lesson = _lessonsService.GetLessonById(lessonId.Value);
                if (lesson != null)
                {
                    if (lesson.LanguageId == SessionHelper.CurrentLanguageId)
                    {
                        return View(lesson);
                    }
                }
            }

            return RedirectToAction("Index");
        }

        #region API

        [HttpPost]
        public ActionResult Login(DAL.User user)
        {
            if (_usersService.ExistsUser(user.Username, user.Password, out user))
            {
                SessionHelper.UserId = user.UserId;
                SessionHelper.User = user;
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Logout()
        {
            if (SessionHelper.Authentificated)
            {
                SessionHelper.UserId = -1;
                SessionHelper.CurrentLanguageId = -1;
                SessionHelper.User = null;

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Register(DAL.User user)
        {
            if (_usersService.RegisterUser(user.Username, user.Email, user.Password, out user))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetLevels(int? languageId)
        {
            if (languageId.HasValue)
            {
                return Json(_languagesService.GetAllLevelsByLanguageId(languageId.Value), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetLessonCategories(int? languageId)
        {
            if (languageId.HasValue)
            {
                return Json(_languagesService.GetAllLessonCategoriesByLanguageId(languageId.Value), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Translate(int? fromLanguageId, int? toLanguageId, string text)
        {
            if (fromLanguageId.HasValue && toLanguageId.HasValue && !String.IsNullOrWhiteSpace(text))
            {
                string translatedText = _languagesService.Translate(fromLanguageId.Value, toLanguageId.Value, text);
                return Json(translatedText, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(String.Empty, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authenticated]
        public ActionResult ShareProgress(int? socialAccountId)
        {
            if (socialAccountId.HasValue)
            {

            }
            else
            {

            }
            return Json(String.Empty, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authenticated]
        public ActionResult LessonSolution(DAL.LessonSolution lessonSolution)
        {
            if (lessonSolution != null)
            {
                lessonSolution.UserId = SessionHelper.UserId;
                var result = _lessonsService.SaveLessonSolution(lessonSolution);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetQuestionsByLessonId(int? lessonId)
        {
            if (lessonId.HasValue)
            {
                var questions = _lessonsService.GetLessonQuestionsByLessonId(lessonId.Value);
                return Json(questions, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}
