﻿using FOLLA.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FOLLA.Controllers
{
    public class StudyBooksController : Controller
    {
        //
        // GET: /StudyBooks/

        private BL.StudyBooksService studyBookService = new BL.StudyBooksService();
        public ActionResult Index()
        {
            if(SessionHelper.CurrentLanguageId == -1) {
                return RedirectToAction("Index", "Home");
            }
            DAL.Language currentLanguage = PagesHelper.GetDefaultLanguage();
            IEnumerable<DAL.StudyBook> listBooks  = studyBookService.GetStudyBooksByLanguageId(currentLanguage.LanguageId);
            ViewBag.studybooks = listBooks;
            ViewBag.size = listBooks.Count();
            return View();
        }


        //
        // GET: /StudyBooks/

        public ActionResult Read(int? bookId)
        {
            if (!bookId.HasValue)
            {
                return RedirectToAction("Index");
            }

            DAL.StudyBook book = studyBookService.GetStudyBookById(bookId.Value); ;
            ViewBag.book = book;
            return View();
        }
    }
}
