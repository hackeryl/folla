﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FOLLA.Helpers
{
    public class PagesHelper
    {
        private static BL.LanguagesService _languagesService = new BL.LanguagesService();
        private static BL.UserService _usersService = new BL.UserService();
        private static readonly DAL.User guest = new DAL.User()
        {
            Username = "guest",
            Score = 0
        };
        private static readonly IEnumerable<DAL.Achievement> guestAchievements = new List<DAL.Achievement>();

        public static void Refresh()
        {
            _languagesService = new BL.LanguagesService();
            _usersService = new BL.UserService();
        }

        public static IEnumerable<DAL.Language> GetAllLanguages()
        {
            return _languagesService.GetAllLanguages();
        }

        public static DAL.Language GetDefaultLanguage()
        {
            DAL.Language lang = null;
            if (SessionHelper.CurrentLanguageId != -1)
            {
                lang = _languagesService.GetLanguageById(SessionHelper.CurrentLanguageId);                
            }
            else
            {
                lang = _languagesService.GetLanguageByName("English");                 
            }
            if (lang == null)
            {
                lang = new DAL.Language()
                {
                    Code = "En",
                    Name = "English",
                    Icon = "English-icon.png"
                };
            }
            return lang;
        }

        public static DAL.User GetCurrentUser()
        {
            if (SessionHelper.Authentificated)
            {
                return _usersService.GetUserById(SessionHelper.UserId);
            }
            else
            {
                return guest;
            }
        }

        public static int GetUserCompletedLessons()
        {
            if (SessionHelper.Authentificated)
            {
                return _usersService.GetNumberOfPassedLessons(SessionHelper.UserId);
            }
            else
            {
                return 0;
            }
        }

        public static int GetUserNotCompletedLessons()
        {
            if (SessionHelper.Authentificated)
            {
                return _usersService.GetNumberOfNotCompletedLessons(SessionHelper.UserId);
            }
            else
            {
                return 0;
            }
        }

        public static int GetUserAccurancy()
        {
            if (SessionHelper.Authentificated)
            {
                return _usersService.GetUserAccurancy(SessionHelper.UserId);
            }
            else
            {
                return 0;
            }
        }

        public static IEnumerable<DAL.Achievement> GetUserAchievements()
        {
            if (SessionHelper.Authentificated)
            {
                return _usersService.GetUserAchievements(SessionHelper.UserId);
            }
            else
            {
                return guestAchievements;
            }
        }

        public static IEnumerable<DAL.SocialNetwork> GetAllSocialNetworks()
        {
            return _usersService.GetAllSocialNetworks();
        }
    }
}