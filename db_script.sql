USE [master]
GO
/****** Object:  Database [folla]    Script Date: 28-Jan-15 04:03:00 ******/
CREATE DATABASE [folla]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'folla', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\folla.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'folla_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\folla_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [folla] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [folla].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [folla] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [folla] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [folla] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [folla] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [folla] SET ARITHABORT OFF 
GO
ALTER DATABASE [folla] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [folla] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [folla] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [folla] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [folla] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [folla] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [folla] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [folla] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [folla] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [folla] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [folla] SET  DISABLE_BROKER 
GO
ALTER DATABASE [folla] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [folla] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [folla] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [folla] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [folla] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [folla] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [folla] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [folla] SET RECOVERY FULL 
GO
ALTER DATABASE [folla] SET  MULTI_USER 
GO
ALTER DATABASE [folla] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [folla] SET DB_CHAINING OFF 
GO
ALTER DATABASE [folla] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [folla] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [folla]
GO
/****** Object:  Table [dbo].[Achievement]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Achievement](
	[AchievementId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Icon] [nvarchar](256) NOT NULL,
	[Score] [int] NOT NULL,
 CONSTRAINT [PK_Achievement] PRIMARY KEY CLUSTERED 
(
	[AchievementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Game]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Game](
	[GameId] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Icon] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](3072) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[ResourceUrl] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[GameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Language]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Language](
	[LanguageId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Icon] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](16) NOT NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lesson]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lesson](
	[LessonId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[LevelId] [int] NOT NULL,
	[LessonCategoryId] [int] NOT NULL,
	[Icon] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Lesson] PRIMARY KEY CLUSTERED 
(
	[LessonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LessonCategory]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LessonCategory](
	[LessonCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Icon] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_LessonCategory] PRIMARY KEY CLUSTERED 
(
	[LessonCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LessonSolution]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LessonSolution](
	[LessonSolutionId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[LessonId] [int] NOT NULL,
	[CorrectAnswers] [int] NOT NULL,
 CONSTRAINT [PK_LessonSolution] PRIMARY KEY CLUSTERED 
(
	[LessonSolutionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Level]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level](
	[LevelId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Icon] [nvarchar](256) NOT NULL,
	[LevelValue] [int] NOT NULL,
 CONSTRAINT [PK_Level] PRIMARY KEY CLUSTERED 
(
	[LevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Question]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[QuestionId] [int] IDENTITY(1,1) NOT NULL,
	[LessonId] [int] NOT NULL,
	[Text] [nvarchar](3072) NOT NULL,
	[Type] [int] NOT NULL,
	[Answer1] [nvarchar](3072) NOT NULL,
	[Answer2] [nvarchar](3072) NOT NULL,
	[Answer3] [nvarchar](3072) NOT NULL,
	[Answer4] [nvarchar](3072) NOT NULL,
	[ResourceUrl] [nvarchar](256) NULL,
	[CorrectAnswerId] [int] NOT NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SocialAccount]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SocialAccount](
	[SocialAccountId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Token] [nvarchar](256) NOT NULL,
	[SocialNetworkId] [int] NOT NULL,
 CONSTRAINT [PK_SocialAccount] PRIMARY KEY CLUSTERED 
(
	[SocialAccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SocialNetwork]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SocialNetwork](
	[SocialNetworkId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ImagePath] [nvarchar](50) NOT NULL,
	[ShareUrl] [nvarchar](4000) NOT NULL,
 CONSTRAINT [PK_SocialNetwork] PRIMARY KEY CLUSTERED 
(
	[SocialNetworkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudyBook]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudyBook](
	[StudyBookId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Icon] [nvarchar](256) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Author] [nvarchar](256) NOT NULL,
	[Year] [int] NOT NULL,
	[Description] [nvarchar](3072) NOT NULL,
	[ResourceUrl] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_StudyBook] PRIMARY KEY CLUSTERED 
(
	[StudyBookId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](128) NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[Score] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserAchievement]    Script Date: 28-Jan-15 04:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAchievement](
	[UserAchievementId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[AchievementId] [int] NOT NULL,
 CONSTRAINT [PK_UserAchievement] PRIMARY KEY CLUSTERED 
(
	[UserAchievementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Achievement] ON 

GO
INSERT [dbo].[Achievement] ([AchievementId], [Name], [Icon], [Score]) VALUES (1, N'FirstLessonCompleted', N'FirstLessonCompleted.png', 0)
GO
INSERT [dbo].[Achievement] ([AchievementId], [Name], [Icon], [Score]) VALUES (2, N'LevelCompleted', N'LevelCompleted.png', 0)
GO
INSERT [dbo].[Achievement] ([AchievementId], [Name], [Icon], [Score]) VALUES (3, N'LessonCategoryCompleted', N'LessonCategoryCompleted.png', 0)
GO
SET IDENTITY_INSERT [dbo].[Achievement] OFF
GO
SET IDENTITY_INSERT [dbo].[Language] ON 

GO
INSERT [dbo].[Language] ([LanguageId], [Name], [Icon], [Code]) VALUES (1, N'English', N'English-icon.png', N'en')
GO
INSERT [dbo].[Language] ([LanguageId], [Name], [Icon], [Code]) VALUES (2, N'Romanian', N'Romania-icon.png', N'ro')
GO
INSERT [dbo].[Language] ([LanguageId], [Name], [Icon], [Code]) VALUES (3, N'Russian', N'Russia-icon.png', N'ru')
GO
INSERT [dbo].[Language] ([LanguageId], [Name], [Icon], [Code]) VALUES (4, N'Deutsch', N'Germany-icon.png', N'de')
GO
INSERT [dbo].[Language] ([LanguageId], [Name], [Icon], [Code]) VALUES (5, N'French', N'France-icon.png', N'fr')
GO
INSERT [dbo].[Language] ([LanguageId], [Name], [Icon], [Code]) VALUES (6, N'Italian', N'Italy-icon.png', N'it')
GO
SET IDENTITY_INSERT [dbo].[Language] OFF
GO
SET IDENTITY_INSERT [dbo].[Lesson] ON 

GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (1, N'Food', 1, 1, 3, N'food.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (7, N'History', 1, 2, 3, N'historical.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (9, N'Geography', 1, 1, 3, N'geografy.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (10, N'Nature', 1, 3, 3, N'nature.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (14, N'Computers', 1, 2, 5, N'computers.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (15, N'Mobiles', 1, 2, 5, N'mobiles.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (16, N'Robots', 1, 3, 5, N'robots.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (17, N'Vocabulary 1', 1, 1, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (18, N'Vocabulary 2', 1, 2, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (19, N'Grammar 1', 1, 2, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (20, N'Grammar 2', 1, 3, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (21, N'Listening 1', 1, 2, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (22, N'Listening 2', 1, 3, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (23, N'Mancare', 2, 1, 3, N'food.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (24, N'Istorie', 2, 2, 3, N'historical.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (25, N'Geografie', 2, 1, 3, N'geografy.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (26, N'Natura', 2, 3, 3, N'nature.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (27, N'Computere', 2, 2, 5, N'computers.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (28, N'Telefoane Mobile', 2, 2, 5, N'mobiles.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (29, N'Roboti', 2, 3, 5, N'robots.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (30, N'Vocabular 1', 2, 1, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (31, N'Vocabular 2', 2, 2, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (32, N'Gramatica 1', 2, 2, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (33, N'Gramatica 2', 2, 3, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (34, N'Ascultare 1', 2, 2, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (35, N'Ascultare 2', 2, 3, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (36, N'еда', 3, 1, 3, N'food.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (37, N'история', 3, 2, 3, N'historical.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (38, N'география', 3, 1, 3, N'geografy.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (39, N'природа', 3, 3, 3, N'nature.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (40, N'компьютеры', 3, 2, 5, N'computers.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (41, N'Мобильные телефоны', 3, 2, 5, N'mobiles.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (42, N'Роботы', 3, 3, 5, N'robots.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (43, N'Словарь 1', 3, 1, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (44, N'Словарь 2', 3, 2, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (45, N'грамматика 1', 3, 2, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (46, N'грамматика 2', 3, 3, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (47, N'прослушивание 1', 3, 2, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (48, N'прослушивание 2', 3, 3, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (49, N'Lebensmittel', 4, 1, 3, N'food.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (50, N'Geschichte', 4, 2, 3, N'historical.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (51, N'Geographie', 4, 1, 3, N'geografy.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (52, N'Natur', 4, 3, 3, N'nature.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (53, N'Computer', 4, 2, 5, N'computers.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (54, N'Mobiles', 4, 2, 5, N'mobiles.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (55, N'Robots', 4, 3, 5, N'robots.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (56, N'Vocabulary 1', 4, 1, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (57, N'Vocabulary 2', 4, 2, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (58, N'Grammar 1', 4, 2, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (59, N'Grammar 2', 4, 3, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (60, N'hören 1', 4, 2, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (61, N'hören 2', 4, 3, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (62, N'nourriture', 5, 1, 3, N'food.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (63, N'histoire', 5, 2, 3, N'historical.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (64, N'géographie', 5, 1, 3, N'geografy.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (65, N'Nature', 5, 3, 3, N'nature.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (66, N'ordinateurs', 5, 2, 5, N'computers.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (67, N'Mobiles', 5, 2, 5, N'mobiles.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (68, N'Robots', 5, 3, 5, N'robots.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (69, N'vocabulaire 1', 5, 1, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (70, N'vocabulaire 2', 5, 2, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (71, N'Grammaire 1', 5, 2, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (72, N'Grammaire 2', 5, 3, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (73, N'Écouter 1', 5, 2, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (74, N'Écouter 2', 5, 3, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (75, N'cibo', 6, 1, 3, N'food.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (76, N'storia', 6, 2, 3, N'historical.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (77, N'geografia', 6, 1, 3, N'geografy.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (78, N'natura', 6, 3, 3, N'nature.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (79, N'Computers', 6, 2, 5, N'computers.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (80, N'Cellulari', 6, 2, 5, N'mobiles.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (81, N'Robots', 6, 3, 5, N'robots.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (82, N'Vocabolario 1', 6, 1, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (83, N'Vocabolario 2', 6, 2, 1, N'vocabulary.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (84, N'Grammar 1', 6, 2, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (85, N'Grammar 2', 6, 3, 2, N'grammar.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (86, N'Ascolto 1', 6, 2, 4, N'listening.png')
GO
INSERT [dbo].[Lesson] ([LessonId], [Name], [LanguageId], [LevelId], [LessonCategoryId], [Icon]) VALUES (87, N'Ascolto 2', 6, 3, 4, N'listening.png')
GO
SET IDENTITY_INSERT [dbo].[Lesson] OFF
GO
SET IDENTITY_INSERT [dbo].[LessonCategory] ON 

GO
INSERT [dbo].[LessonCategory] ([LessonCategoryId], [Name], [Icon]) VALUES (1, N'Vocabulary', N'vocabulary.png')
GO
INSERT [dbo].[LessonCategory] ([LessonCategoryId], [Name], [Icon]) VALUES (2, N'Grammar', N'grammar.png')
GO
INSERT [dbo].[LessonCategory] ([LessonCategoryId], [Name], [Icon]) VALUES (3, N'General Knowledges', N'general_knnoledges.png')
GO
INSERT [dbo].[LessonCategory] ([LessonCategoryId], [Name], [Icon]) VALUES (4, N'Listening', N'listening.png')
GO
INSERT [dbo].[LessonCategory] ([LessonCategoryId], [Name], [Icon]) VALUES (5, N'Technology', N'technology.png')
GO
SET IDENTITY_INSERT [dbo].[LessonCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[LessonSolution] ON 

GO
INSERT [dbo].[LessonSolution] ([LessonSolutionId], [UserId], [LessonId], [CorrectAnswers]) VALUES (1, 1, 9, 5)
GO
SET IDENTITY_INSERT [dbo].[LessonSolution] OFF
GO
SET IDENTITY_INSERT [dbo].[Level] ON 

GO
INSERT [dbo].[Level] ([LevelId], [Name], [Icon], [LevelValue]) VALUES (1, N'Beginner', N'beginner.png', 1)
GO
INSERT [dbo].[Level] ([LevelId], [Name], [Icon], [LevelValue]) VALUES (2, N'Intermediate', N'intermediate.png', 2)
GO
INSERT [dbo].[Level] ([LevelId], [Name], [Icon], [LevelValue]) VALUES (3, N'Advances', N'advanced.png', 3)
GO
SET IDENTITY_INSERT [dbo].[Level] OFF
GO
SET IDENTITY_INSERT [dbo].[Question] ON 

GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (1, 9, N'Which is the greatest continent?', 1, N'North America', N'South America', N'Asia', N'Africa', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (2, 9, N'Which is the smallest continent?', 1, N'North America', N'South America', N'Europe', N'Australia', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (3, 9, N'What country represents the following image?', 3, N'Italy', N'France', N'Romania', N'Moldova', N'moldova_map.png', 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (4, 9, N'Which of the following images represents Moldova?', 2, N'moldova_flag.png', N'italy_map.png', N'france_map.png', N'usa_map.png', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (5, 9, N'What does the following sound says?', 4, N'Water', N'Sea', N'Lake', N'River', N'water.mp3', 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (6, 9, N'Which image corresponds to the following sound?', 5, N'lion.png', N'tiger.png', N'cat.png', N'dog.png', N'tiger.mp3', 2)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (7, 17, N'Where is the ERROR?
Are there any tigers in Sweden?
No aren''t any.', 1, N'Are', N'any tigers in Sweden?', N'No aren''t', N'any.', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (9, 17, N'Choose the CORRECT RESPONSE:

Where are you going?', 1, N'I am going to my mother''s house.', N'I going at work.', N'I are going home.', N'I go to the store.', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (10, 17, N'Fill in the blank:
 
With that red hat, you really ______ out in the crowd.
', 1, N'turn', N'throw', N'notice', N'stand', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (11, 17, N'Choose the CORRECT RESPONSE:
 Where were you yesterday?', 1, N'I were with your sister at the movies.', N'I were with your sister at the movies.', N'I wasn''t in town.', N'I wasn''t to the office.', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (13, 17, N'Fill in the blank:
 
Taipei is ______ Taiwan.
', 1, N'of', N'at', N'in', N'from', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (14, 17, N'Choose the CORRECT RESPONSE:
 
What were Thomas and Miguel doing when you arrived?

', 1, N'They were listening music.', N'Thomas and Miguel talking.', N'They played cards.', N'Thomas and Miguel were preparing dinner.', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (16, 17, N'Fill in the blank:
 
Junko will be late. She must drive ______ Kobe to Tokyo.', 1, N'to', N'of', N'onto', N'from', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (17, 17, N'Fill in the blank:
 
You can use my car ______ tomorrow.
', 1, N'yet', N'since', N'until', N'around', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (18, 17, N'Fill in the blank:
 
I''m sorry we are so late; our car ______ down on the highway.
', 1, N'breaking', N'broke', N'broken', N'break', NULL, 2)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (19, 17, N'Fill in the blank:
 
______ I saw the president in the restaurant.', 1, N'I thinks', N'Did thought', N'Thought', N'I thought', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (20, 17, N'Choose the CORRECT RESPONSE:
 
<br>How long was the trip to the mountains?', 1, N'It took only four hours.', N'It was about two hundred kilometers', N'It''s two weeks.', N'We gone for the whole summer.', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (21, 17, N'Fill in the blank:
 
Crabs are strange animals because they walk ______.
', 1, N'below', N'beside', N'to side', N'sideways', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (22, 17, N'CHANGE this verb TO PAST TENSE: set', 1, N'setten', N'sat', N'sit', N'set', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (23, 17, N'CHANGE this verb TO PAST TENSE: eat', 1, N'eat', N'eaten', N'ate', N'eated', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (24, 17, N'Which is CORRECT?', 1, N'Takes money to make money.', N'Money doesn''t grow of trees.', N'It''s easy to be generous with somebody''s else money.', N'Time is money.', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (26, 18, N'That is a bird. ............. bird has two wings.', 1, N'-', N'A', N'An', N'The', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (27, 18, N'Do you drink milk every day?', 1, N'No, you do.', N'No, you don''t.', N'Yes, you do.', N'Yes, I do.', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (28, 18, N'What ............. you have for lunch?', 1, N'does', N'are', N'do', N'have', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (29, 18, N'I''m hungry. I want two ............. of chicken.', 1, N'glasses', N'plates', N'bowls', N'pieces', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (30, 18, N'hese are ............. .', 1, N'oxen', N'oxes', N'an oxen', N'a oxen', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (31, 18, N'He wants ............. sugar.', 1, N'a', N'any', N'the', N'some', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (32, 18, N'That is ............. .', 1, N'an egg', N'eggs', N'egg', N'a egg', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (33, 18, N'An elephant ............. four legs.', 1, N'have', N'is', N'the', N'has', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (34, 19, N'John is the ___ runner in our class.', 1, N'faster', N'fast', N'fastest', N'fats', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (35, 19, N'I ___ tired, so I went to bed early.', 1, N'where', N'am', N'was', N'are', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (36, 19, N'She wants to ___ a nurse.', 1, N'be', N'is', N'have', N'si', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (37, 19, N'When did you go to that restaurant?', 1, N'About 30 minutes.', N'Last night.', N'Speghetti.', N'With Jane.', NULL, 2)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (38, 19, N'My friend ___ a party every month.', 1, N'having', N'have', N'has', N'is', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (39, 19, N'There ___ a tall building near the park.', 1, N'are', N'is', N'went', N'am', NULL, 2)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (40, 20, N'Are your shoes new?', 1, N'No, they''re old.', N'No, it''s old.', N'No, he''s old.', N'No, i''m old.', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (41, 20, N'What kind of work do you do?', 1, N'I worked for two hours.', N'I''m a piano teacher.', N'I work every day.', N'I worked every day.', NULL, 2)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (42, 20, N'Can you help me? I _____ a post office. ', 1, N'am looking for', N' look for', N'am look for', N'looking for', NULL, 1)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (43, 20, N'Tomorrow the Queen _____ open a new hospital. ', 1, N'is going', N'will going to', N'is going to', N'will to', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (44, 20, N'What _____? I work in a book shop. ', 1, N'are you doing', N'do you', N'are you do', N'do you do', NULL, 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (45, 20, N'Where _____ yesterday? ', 1, N'you was', N'are you', N' were you', N'did you were', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (46, 20, N' Last month he _____ three large fish. ', 1, N'catch', N'catched', N'caught', N'did catch', NULL, 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (47, 21, N'What do you hear in this sound?', 4, N'When we went in, they listened to the radio. ', N'When we went in, they listening to the radio. ', N'When we went in, they were listening to the radio. ', N'When we went in, they were listen to the radio. ', N'listen1.mp3', 3)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (48, 21, N'What do you hear in this sound?', 4, N'Do you like Paris? I don''t know. I never went there. ', N'Do you like Paris? I don''t know. I didn''t go there. ', N'Do you like Paris? I don''t know. I never gone there. ', N'Do you like Paris? I don''t know. I haven''t been there. ', N'listen2.mp3', 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (49, 21, N'What do you hear in this sound?', 4, N'I''m a teacher. I was a teacher for twelve years. ', N'I''m a teacher. I have been a teacher for twelve years. ', N'I''m a teacher. I am a teacher for twelve years. ', N'I''m a teacher. I am being a teacher for twelve years. ', N'listen3.mp3', 2)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (50, 21, N'What do you hear in this sound?', 4, N'He''s the best dancer. He dances very beautiful than anyone else.', N'He''s the best dancer. He dances more beautiful than anyone else.', N'He''s the best dancer. He dances  much more beautiful than anyone else.', N'He''s the best dancer. He dance more beautifully than anyone else.', N'listen4.mp3', 2)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (51, 22, N'What do you hear in this sound?', 4, N'They don''t need any help. They can do it theirself . ', N'They don''t need any help. They can do it theirselves . ', N'They don''t need any help. They can do it their own. ', N'They don''t need any help. They can do itthemselves . ', N'listen5.mp3', 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (52, 22, N'What do you hear in this sound?', 4, N'It''s wrong. You shouldn''t to do it.', N'It''s wrong. You needn''t do it.', N'It''s wrong. You don''t have to do it.', N'It''s wrong. You shouldn''t do it.', N'listen6.mp3', 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (53, 22, N'What do you hear in this sound?', 4, N'I''m tired to answer questions. ', N'I''m tired that I answer questions. ', N'I''m tired because of to answer questions. ', N'I''m tired of answering questions. ', N'listen7.mp3', 4)
GO
INSERT [dbo].[Question] ([QuestionId], [LessonId], [Text], [Type], [Answer1], [Answer2], [Answer3], [Answer4], [ResourceUrl], [CorrectAnswerId]) VALUES (54, 22, N'What do you hear in this sound?', 4, N'I don''t know  because he did it. ', N'I don''t know why did he it. ', N'I don''t know why did he do it. ', N'I don''t know why he did it. ', N'listen8.mp3', 4)
GO
SET IDENTITY_INSERT [dbo].[Question] OFF
GO
SET IDENTITY_INSERT [dbo].[SocialNetwork] ON 

GO
INSERT [dbo].[SocialNetwork] ([SocialNetworkId], [Name], [ImagePath], [ShareUrl]) VALUES (2, N'Twitter', N'twitter.png', N'https://twitter.com/intent/tweet?text=My%20current%20score%20on%20folla.somee.com%20is%20{0}.%20Think%20you%20ca%20do%20better?')
GO
INSERT [dbo].[SocialNetwork] ([SocialNetworkId], [Name], [ImagePath], [ShareUrl]) VALUES (3, N'Linkedin', N'linkedin.png', N'http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Ffolla.somee.com&title=Folla&source=Folla&summary=My%20current%20score%20on%20folla.somee.com%20is%20{0}.%20Think%20you%20ca%20do%20better?')
GO
INSERT [dbo].[SocialNetwork] ([SocialNetworkId], [Name], [ImagePath], [ShareUrl]) VALUES (4, N'Blogger', N'blog.png', N'https://www.blogger.com/blog-this.g?u=http%3A%2F%2Ffolla.somee.com&n=Folla&t=My%20current%20score%20on%20folla.somee.com%20is%20{0}.%20Think%20you%20ca%20do%20better%3F')
GO
SET IDENTITY_INSERT [dbo].[SocialNetwork] OFF
GO
SET IDENTITY_INSERT [dbo].[StudyBook] ON 

GO
INSERT [dbo].[StudyBook] ([StudyBookId], [Name], [Icon], [LanguageId], [Author], [Year], [Description], [ResourceUrl]) VALUES (1, N'English Grammar Tests', N'book.png', 1, N'Ad-Hoc', 2014, N'Hard training', N'http://www.noorsa.net/files/file/English%20Grammar%20Tests.pdf')
GO
INSERT [dbo].[StudyBook] ([StudyBookId], [Name], [Icon], [LanguageId], [Author], [Year], [Description], [ResourceUrl]) VALUES (3, N'English Grammar Tests Vol.2', N'book.png', 1, N'Ad-Hoc', 2015, N'Hard Training', N'http://www.really-learn-english.com/support-files/free-english-grammar-test-for-download.pdf')
GO
INSERT [dbo].[StudyBook] ([StudyBookId], [Name], [Icon], [LanguageId], [Author], [Year], [Description], [ResourceUrl]) VALUES (4, N'Grammar Review Book Quizzes', N'book.png', 1, N'Ad-Hoc', 2013, N'Hard Training', N'http://www.prolinguaassociates.com/Grammar_Review_Book/Grammar%20Review%20Book%20Quizzes.pdf')
GO
SET IDENTITY_INSERT [dbo].[StudyBook] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

GO
INSERT [dbo].[User] ([UserId], [Username], [Email], [Password], [Score]) VALUES (1, N'vasile', N'vasea.pojoga@gmail.com', N'vasile', 5)
GO
INSERT [dbo].[User] ([UserId], [Username], [Email], [Password], [Score]) VALUES (2, N'augustin', N'augustin.tataru@info.uaic.ro', N'augustin', 0)
GO
INSERT [dbo].[User] ([UserId], [Username], [Email], [Password], [Score]) VALUES (3, N'ionel', N'ionel@gmail.com', N'ionel', 0)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[UserAchievement] ON 

GO
INSERT [dbo].[UserAchievement] ([UserAchievementId], [UserId], [AchievementId]) VALUES (1, 1, 1)
GO
INSERT [dbo].[UserAchievement] ([UserAchievementId], [UserId], [AchievementId]) VALUES (2, 1, 3)
GO
SET IDENTITY_INSERT [dbo].[UserAchievement] OFF
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_Language] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([LanguageId])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_Language]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Language] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([LanguageId])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Language]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_LessonCategory] FOREIGN KEY([LessonCategoryId])
REFERENCES [dbo].[LessonCategory] ([LessonCategoryId])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_LessonCategory]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Level] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Level] ([LevelId])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Level]
GO
ALTER TABLE [dbo].[LessonSolution]  WITH CHECK ADD  CONSTRAINT [FK_LessonSolution_Lesson] FOREIGN KEY([LessonId])
REFERENCES [dbo].[Lesson] ([LessonId])
GO
ALTER TABLE [dbo].[LessonSolution] CHECK CONSTRAINT [FK_LessonSolution_Lesson]
GO
ALTER TABLE [dbo].[LessonSolution]  WITH CHECK ADD  CONSTRAINT [FK_LessonSolution_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[LessonSolution] CHECK CONSTRAINT [FK_LessonSolution_User]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Lesson] FOREIGN KEY([LessonId])
REFERENCES [dbo].[Lesson] ([LessonId])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Lesson]
GO
ALTER TABLE [dbo].[SocialAccount]  WITH CHECK ADD  CONSTRAINT [FK_SocialAccount_SocialNetwork] FOREIGN KEY([SocialNetworkId])
REFERENCES [dbo].[SocialNetwork] ([SocialNetworkId])
GO
ALTER TABLE [dbo].[SocialAccount] CHECK CONSTRAINT [FK_SocialAccount_SocialNetwork]
GO
ALTER TABLE [dbo].[SocialAccount]  WITH CHECK ADD  CONSTRAINT [FK_SocialAccount_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[SocialAccount] CHECK CONSTRAINT [FK_SocialAccount_User]
GO
ALTER TABLE [dbo].[StudyBook]  WITH CHECK ADD  CONSTRAINT [FK_StudyBook_Language] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([LanguageId])
GO
ALTER TABLE [dbo].[StudyBook] CHECK CONSTRAINT [FK_StudyBook_Language]
GO
ALTER TABLE [dbo].[UserAchievement]  WITH CHECK ADD  CONSTRAINT [FK_UserAchievement_Achievement] FOREIGN KEY([AchievementId])
REFERENCES [dbo].[Achievement] ([AchievementId])
GO
ALTER TABLE [dbo].[UserAchievement] CHECK CONSTRAINT [FK_UserAchievement_Achievement]
GO
ALTER TABLE [dbo].[UserAchievement]  WITH CHECK ADD  CONSTRAINT [FK_UserAchievement_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserAchievement] CHECK CONSTRAINT [FK_UserAchievement_User]
GO
USE [master]
GO
ALTER DATABASE [folla] SET  READ_WRITE 
GO
