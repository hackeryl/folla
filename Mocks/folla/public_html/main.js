/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    onDomReady();
    function onDomReady() {
        //add listeners
        $("#language-holder .img-item").on('click', function (event) {
            event.preventDefault();

            $('#language-holder').toggleClass('hidden');
            $('#topic-holder').toggleClass('hidden');
            $('#level-holder').toggleClass('hidden');
            $('#pageTitle').text('Where should we start?');
        });
    }

    //language widget toggle
    $('.iconFlag').click(function () {
        $('.language-widget').toggle();
    });


    //user widget toggle
    $('.iconUser').click(function () {
        $('.user-widget').toggle();
    });

    //user widget toggle
    $('.iconDictionary').click(function () {
        $('.dictionary-widget').toggle();
    });

    $('#user-widget .show-sign-in-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-in-container').first().toggleClass('hidden');
    });

    $('#user-widget .sign-in-container .cancel-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-in-container').first().toggleClass('hidden');
    });

    $('#user-widget .show-sign-up-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-up-container').first().toggleClass('hidden');
    });

    $('#user-widget .sign-up-container .cancel-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-up-container').first().toggleClass('hidden');
    });

    $('#user-widget .sign-in-btn').on('click', function (event) {
        event.preventDefault();
        //code for sign in
    });

    $('#user-widget .sign-up-btn').on('click', function (event) {
        event.preventDefault();
        //code for sign up
    });


    //stop propagation of onclick event on widget's area
    $('.widget').on('click', function (event) {
        event.stopPropagation();
    });
})(jQuery);
